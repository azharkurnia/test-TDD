from .views import index, add_todo, delete_todo
from django.conf.urls import url

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_todo', add_todo, name='add_todo'),
	url(r'^delete/(?P<pk>\d+)/$', delete_todo, name='delete_todo'),
]
