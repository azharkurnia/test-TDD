from django.test import TestCase, Client
from .api_csui_helper.csui_helper import CSUIhelper


class Lab7UnitTest(TestCase):

    def test_lab_7_url_is_exist(self):
        response = Client().get('/lab-7/get-friend-list/next-page/')
        self.assertEqual(response.status_code, 200)
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)
        response = Client().get('/lab-7/get-friend-list/previous-page/')
        self.assertEqual(response.status_code, 200)
        response = Client().get('/lab-7/get-friend-list/next-page/')
        self.assertEqual(response.status_code, 200)
        response = Client().get('/lab-7/get-friend-list/previous-page/')
        self.assertEqual(response.status_code, 200)
        response = Client().get('/lab-7/get-friend-list/')
        self.assertEqual(response.status_code, 200)

        data = {
            'name': 'Coba',
            'npm': 12345
        }

        response = Client().post('/lab-7/add-friend/', data)
        self.assertEqual(response.status_code, 200)

        response = Client().post('/lab-7/add-friend/', data)
        self.assertEqual(response.status_code, 400)

        response = Client().get('/lab-7/get-friend-list-json/')
        self.assertEqual(response.status_code, 200)

        response = Client().get('/lab-7/delete-friend/12345/')
        self.assertEqual(response.status_code, 302)

        response = Client().post('/lab-7/validate-npm/', {'npm': 12345})
        self.assertEqual(response.status_code, 200)

    def test_csui_helper(self):
        csui_helper = CSUIhelper(None, None)
        csui_helper.get_client_id()
        csui_helper.get_auth_param_dict()
